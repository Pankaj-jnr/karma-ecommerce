export const paths = {
  home: "/",
  shop: "/shop",
  blog: "/blog",
  contact: "/contact",
  login: "/login",
  register: "/register",
  productDetails: "/prodct-details/:id",
  cart: "/product/cart",
  checkOut: "/product/checkout",
  logOut: "/logout",
  pageNotFound: "*",
  searchResult: "/search/:cat",
};
