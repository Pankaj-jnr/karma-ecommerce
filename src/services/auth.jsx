import { usersData } from "../fake-db/users";

export const authService = {
  login: (data) => {
    const { email, password } = data;
    const checkUser = usersData.find(
      (matchdata) =>
        matchdata.email === email && matchdata.password === password
    );
    if (checkUser) {
      return {
        status: true,
        message: `welcome ${data.email}`,
        data: {
          email: data.email,
        },
      };
    } else {
      return {
        status: false,
        message: "Email and password is wrong",
        data: {},
      };
    }
  },
};
