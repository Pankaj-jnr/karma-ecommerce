import { authService } from "../services/auth";
import { shopServices } from "./shop";
import { categoryServices } from "./category";

export default { ...authService, ...shopServices, ...categoryServices };
