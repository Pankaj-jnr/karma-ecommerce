import { shop } from "../fake-db/shop";
export const shopServices = {
  getAllProducts: () => {
    return shop;
  },

  singleProduct: (id) => {
    console.log("type of ", typeof id);
    const product = shop.find((product) => product.id == id);
    return product;
  },
};
