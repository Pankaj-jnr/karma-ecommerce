import { paths } from "../config/PathConfig";
import { lazy } from "react";
const KMRoutes = [
  {
    path: paths.home,
    element: lazy(() => import("../views/pages/home/Home")),
  },
  {
    path: paths.shop,
    element: lazy(() => import("../views/pages/shop/Shop")),
  },

  {
    path: paths.blog,
    element: lazy(() => import("../views/pages/Blog")),
  },
  {
    path: paths.login,
    element: lazy(() => import("../views/pages/auth/Login")),
  },

  {
    path: paths.logOut,
    element: lazy(() => import("../views/pages/auth/LogOut")),
  },

  {
    path: paths.register,
    element: lazy(() => import("../views/pages/auth/Register")),
  },

  {
    path: paths.productDetails,
    element: lazy(() => import("../views/pages/shop/ProductDetails")),
  },
  {
    path: paths.cart,
    element: lazy(() => import("../views/pages/shop/Cart")),
  },

  {
    path: paths.checkOut,
    element: lazy(() => import("../views/pages/shop/CheckOut")),
  },

  {
    path: paths.pageNotFound,
    element: lazy(() => import("../views/pages/PageNotFound")),
  },

  {
    path: paths.searchResult,
    element: lazy(() => import("../views/pages/shop/SearchProduct")),
  },
];

export default KMRoutes;
