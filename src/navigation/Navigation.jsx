import { paths } from "../config/PathConfig";

const nav = [
  {
    title: "home",
    id: "home",
    path: paths.home,
    hidden: false,
    auth: false,
  },
  {
    title: "shop",
    id: "shop",
    path: paths.shop,
    hidden: false,
    auth: false,
  },
  {
    title: "blog",
    id: "blog",
    path: paths.blog,
    hidden: false,
    auth: false,
  },
  {
    title: "Register",
    id: "register",
    path: paths.register,
    hidden: false,
    auth: true,
    checkAuth: false,
  },
  {
    title: "logout",
    id: "logout",
    hidden: false,
    auth: true,
    checkAuth: true,
    path: paths.logOut,
  },
  {
    title: "login",
    id: "login",
    path: paths.login,
    hidden: true,
    auth: true,
    checkAuth: false,

    // children: [
    //   {
    //     title: "404",
    //     id: "register",
    //     path: "/",
    //     hidden: false,
    //   },
    // ],
  },
];
export default nav;
