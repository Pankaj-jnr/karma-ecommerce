import { Fragment, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { paths } from "../../../config/PathConfig";
import { logOutHandle } from "../../../store/auth";
const LogOut = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(logOutHandle());
    navigate(paths.login);
  }, []);

  return <Fragment></Fragment>;
};

export default LogOut;
