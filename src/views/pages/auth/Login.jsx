import { Fragment } from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import BreadCrumb from "../../../components/BreadCrumb";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import services from "../../../services/services";
import useToast from "../../../utility/hooks/useToast";
import { useDispatch } from "react-redux";
import { loginHandle } from "../../../store/auth";

const Login = () => {
  const navigate = useNavigate(); /// redirection
  const toastMessage = useToast();
  const location = useLocation();
  console.log(location, "location");
  // const selector = useSelector((state) => state.auth.auth);
  const dispatch = useDispatch();

  ///validation
  const schema = yup.object().shape({
    email: yup
      .string()
      .email("Email is not valid")
      .required("Email is required"),
    password: yup
      .string()
      .min(6, "Enter at least 6 characters")
      .max(15, "Enter less than 15 characters")
      .required("Password is required"),
  });

  const {
    register: login,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    const apiResponse = services.login(data); //api response
    toastMessage(apiResponse);
    if (apiResponse.status) {
      const userAllData = apiResponse.data;
      console.log(userAllData, "sdfsfsdf");
      dispatch(loginHandle(apiResponse.data));

      console.log(location.state, "location state");
      if (location.state) {
        navigate(location.state.previewPath);
      } else {
        navigate("/");
      }
    } else {
    }
    reset();
  };
  return (
    <Fragment>
      {/* <!-- Start BreadCrumb Area --> */}
      <BreadCrumb
        breadCrumbTitle="Login"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
      {/* <!-- End BreadCrumb Area --> */}

      {/* <!--================Login Box Area =================--> */}
      <section className="login_box_area section_gap">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="login_box_img">
                <img
                  className="img-fluid"
                  src="assets/images/login/login.jpg"
                  alt=""
                />
                <div className="hover">
                  <h4>New to our website?</h4>
                  <p>
                    There are advances being made in science and technology
                    everyday, and a good example of this is the
                  </p>
                  <NavLink to="/register" className="primary-btn">
                    {" "}
                    Create an Account
                  </NavLink>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="login_form_inner">
                <h3>Log in to enter</h3>
                <form onSubmit={handleSubmit(onSubmit)} className="login_form">
                  <div className="col-md-12 form-group">
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      placeholder="Email addresss"
                      {...login("email", { required: true })}
                    />
                    <p className="error">{errors.email?.message}</p>
                  </div>
                  <div className="col-md-12 form-group">
                    <input
                      type="password"
                      className="form-control"
                      id="password"
                      name="password"
                      placeholder="Password"
                      {...login("password", { reqired: true })}
                    />
                    <p className="error">{errors.password?.message}</p>
                  </div>
                  {/* <div class="col-md-12 form-group">
								<div class="creat_account">
									<input type="checkbox" id="f-option2" name="selector" />
									<label for="f-option2">Keep me logged in</label>
								</div>
							</div> */}
                  <div className="col-md-12 form-group">
                    <button
                      type="submit"
                      value="submit"
                      className="primary-btn"
                    >
                      Log In
                    </button>
                    {/* <a href="#">Forgot Password?</a> */}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!--================End Login Box Area =================--> */}
    </Fragment>
  );
};

export default Login;
