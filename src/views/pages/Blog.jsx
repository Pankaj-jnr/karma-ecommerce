import { Fragment } from "react";
import BreadCrumb from "../../components/BreadCrumb";
const Blog = () => {
    return (<Fragment>
            <BreadCrumb breadCrumbTitle='Blog' breadCrumbParent='Home' breadCrumbPath='/'/>
    </Fragment> );
}
 
export default Blog;