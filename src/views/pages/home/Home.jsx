import { Fragment } from "react";
import HomeProduct from "../shop/HomeProduct";
import Slider from "./Slider";

const Home = () => {
  return (
    <Fragment>
      <Slider />
      {/* <!-- start features Area --> */}
      <section class="features-area section_gap">
        <div class="container">
          <div class="row features-inner">
            {/* <!-- single features --> */}
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="single-features">
                <div class="f-icon">
                  <img src="assets/images/features/f-icon1.png" />
                </div>
                <h6>Free Delivery</h6>
                <p>Free Shipping on all order</p>
              </div>
            </div>
            {/* <!-- single features --> */}
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="single-features">
                <div class="f-icon">
                  <img src="assets/images/features/f-icon2.png" alt="" />
                </div>
                <h6>Return Policy</h6>
                <p>Free Shipping on all order</p>
              </div>
            </div>
            {/* <!-- single features --> */}
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="single-features">
                <div class="f-icon">
                  <img src="assets/images/features/f-icon3.png" alt="" />
                </div>
                <h6>24/7 Support</h6>
                <p>Free Shipping on all order</p>
              </div>
            </div>
            {/* <!-- single features --> */}
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="single-features">
                <div class="f-icon">
                  <img src="assets/images/features/f-icon4.png" alt="" />
                </div>
                <h6>Secure Payment</h6>
                <p>Free Shipping on all order</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- end features Area --> */}

      {/* <!-- Start category Area --> */}
      <section class="category-area">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
              <div class="row">
                <div class="col-lg-8 col-md-8">
                  <div class="single-deal">
                    <div class="overlay"></div>
                    <img
                      class="img-fluid w-100"
                      src="assets/images/category/c1.jpg"
                      alt=""
                    />
                    <a
                      href="assets/images/category/c1.jpg"
                      class="img-pop-up"
                      target="_blank"
                    >
                      <div class="deal-details">
                        <h6 class="deal-title">Sneaker for Sports</h6>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4">
                  <div class="single-deal">
                    <div class="overlay"></div>
                    <img
                      class="img-fluid w-100"
                      src="assets/images/category/c2.jpg"
                      alt=""
                    />
                    <a
                      href="assets/images/category/c2.jpg"
                      class="img-pop-up"
                      target="_blank"
                    >
                      <div class="deal-details">
                        <h6 class="deal-title">Sneaker for Sports</h6>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4">
                  <div class="single-deal">
                    <div class="overlay"></div>
                    <img
                      class="img-fluid w-100"
                      src="assets/images/category/c3.jpg"
                      alt=""
                    />
                    <a
                      href="assets/images/category/c3.jpg"
                      class="img-pop-up"
                      target="_blank"
                    >
                      <div class="deal-details">
                        <h6 class="deal-title">Product for Couple</h6>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-lg-8 col-md-8">
                  <div class="single-deal">
                    <div class="overlay"></div>
                    <img
                      class="img-fluid w-100"
                      src="assets/images/category/c4.jpg"
                      alt=""
                    />
                    <a
                      href="assets/images/category/c4.jpg"
                      class="img-pop-up"
                      target="_blank"
                    >
                      <div class="deal-details">
                        <h6 class="deal-title">Sneaker for Sports</h6>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <div class="single-deal">
                <div class="overlay"></div>
                <img
                  class="img-fluid w-100"
                  src="assets/images/category/c5.jpg"
                  alt=""
                />
                <a
                  href="assets/images/category/c5.jpg"
                  class="img-pop-up"
                  target="_blank"
                >
                  <div class="deal-details">
                    <h6 class="deal-title">Sneaker for Sports</h6>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End category Area --> */}

      {/* <!-- start product Area --> */}
      <section className="active-product-area section_gap">
        <div className="single-product-slider">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-6 text-center">
                <div className="section-title">
                  <h1>Latest Products</h1>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </p>
                </div>
              </div>
            </div>
            <HomeProduct />
          </div>
        </div>
      </section>
    </Fragment>
  );
};

export default Home;
