import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import services from "../../../services/services";
import { Fragment, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getProducts } from "../../../store/productSlice";
const Slider = () => {
  const [products, setproducts] = useState([]);
  console.log(products, "products");
  const dispatch = useDispatch();
  useEffect(() => {
    const dbProducts = services.getAllProducts();
    dispatch(getProducts(dbProducts));
    setproducts(dbProducts);
  }, [products]);

  return (
    <section class="banner-area">
      <div class="container">
        <OwlCarousel className="owl-theme" loop margin={10} nav items={1}>
          {/* {products.map((item) => {
            return ( */}
          <Fragment>
            <div class="item">
              <div class="row single-slide align-items-center d-flex">
                <div class="col-lg-5 col-md-6">
                  <div class="banner-content">
                    <h1>
                      Nike New <br /> Collection!
                    </h1>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                    </p>
                    <div class="add-bag d-flex align-items-center">
                      <a class="add-btn" href="">
                        <span class="lnr lnr-cross"></span>
                      </a>
                      <span class="add-text text-uppercase">Add to Bag</span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="banner-img">
                    <img
                      class="img-fluid"
                      src="assets/images/banner/banner-img.png"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row single-slide align-items-center d-flex">
                <div class="col-lg-5 col-md-6">
                  <div class="banner-content">
                    <h1>
                      Nike New <br /> Collection!
                    </h1>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                    </p>
                    <div class="add-bag d-flex align-items-center">
                      <a class="add-btn" href="">
                        <span class="lnr lnr-cross"></span>
                      </a>
                      <span class="add-text text-uppercase">Add to Bag</span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-7">
                  <div class="banner-img">
                    <img
                      class="img-fluid"
                      src="assets/images/banner/banner-img.png"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </Fragment>

          {/* })} */}
        </OwlCarousel>
      </div>
    </section>
  );
};

export default Slider;
