import { Fragment } from "react";
import BreadCrumb from "../../components/BreadCrumb";

const PageNotFound = () => {
  return (
    <Fragment>
      {" "}
      <BreadCrumb
        breadCrumbTitle="Page Not Found"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
    </Fragment>
  );
};

export default PageNotFound;
