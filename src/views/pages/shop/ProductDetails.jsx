import { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import BreadCrumb from "../../../components/BreadCrumb";
import services from "../../../services/services";
import { CartAction } from "../../../store/cartSlice";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import { getProducts } from "../../../store/productSlice";

const ProductDetails = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({});
  const dispatch = useDispatch();
  const cartProduct = useSelector((state) => state.cart.cartItems);
  const [disableCartBtn, setdisableCartBtn] = useState(false);

  useEffect(() => {
    const productdb = services.getAllProducts();
    dispatch(getProducts({ products: productdb }));
    setProduct(services.singleProduct(id));
    // console.log(services.singleProduct(id), "product id");
  }, []);
  const addtoCartHandler = (product) => {
    if (cartProduct.length) {
      const cartItem = cartProduct.find((item) => item.id == product.id);
      if (cartItem) {
        if (cartItem.quantity < 3) {
          dispatch(CartAction.addToCart(product));
          toast.success("Your item has been added on cart");
        } else {
          setdisableCartBtn(true);
          toast.error("You can't add more than 3 items");
        }
      } else {
        dispatch(CartAction.addToCart(product)); /// when item not exist
      }
    } else {
      dispatch(CartAction.addToCart(product)); /// if item
    }
  };
  return (
    <Fragment>
      <BreadCrumb
        breadCrumbTitle="Product Details"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
      {/* {product.map((productItem) => {
        console.log(productItem, "product items");
      })} */}
      <div class="product_image_area">
        <div class="container">
          <div class="row s_product_inner">
            <div class="col-lg-6">
              <div class="s_Product_carousel">
                <div class="single-prd-item">
                  <img
                    class="img-fluid"
                    src="/assets/images/category/s-p1.jpg"
                    alt=""
                  />
                </div>
                {/* <div class="single-prd-item">
                  <img
                    class="img-fluid"
                    src="/assets/images/category/s-p1.jpg"
                    alt=""
                  />
                </div>
                <div class="single-prd-item">
                  <img
                    class="img-fluid"
                    src="/assets/images/category/s-p1.jpg"
                    alt=""
                  />
                </div> */}
              </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
              <div class="s_product_text">
                <h3>Faded SkyBlu Denim Jeans</h3>
                <h2>$149.99</h2>
                <ul class="list">
                  <li>
                    <a class="active" href="#">
                      <span>Category</span> : Household
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <span>Availibility</span> : In Stock
                    </a>
                  </li>
                </ul>
                {/* <p>{product.description}</p> */}
                <div class="product_count">
                  {/* <label for="qty">Quantity:</label>
                  <input
                    type="text"
                    name="qty"
                    id="sst"
                    maxlength="12"
                    value="1"
                    title="Quantity:"
                    class="input-text qty"
                  /> */}
                  {/* <button class="increase items-count" type="button">
                    <i class="lnr lnr-chevron-up"></i>
                  </button>
                  <button
                    onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                    class="reduced items-count"
                    type="button"
                  >
                    <i class="lnr lnr-chevron-down"></i>
                  </button> */}
                </div>
                <div class="card_area d-flex align-items-center">
                  <button
                    disabled={disableCartBtn}
                    class="primary-btn"
                    onClick={() => addtoCartHandler(product)}
                  >
                    Add to Cart
                  </button>
                  <a class="icon_btn" href="#">
                    <i class="lnr lnr lnr-diamond"></i>
                  </a>
                  <a class="icon_btn" href="#">
                    <i class="lnr lnr lnr-heart"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
    </Fragment>
  );
};

export default ProductDetails;
