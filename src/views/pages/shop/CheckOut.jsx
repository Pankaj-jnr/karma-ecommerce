import { Fragment, useEffect } from "react";
import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import BreadCrumb from "../../../components/BreadCrumb";
import { paths } from "../../../config/PathConfig";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-toastify";
const CheckOut = () => {
  const navigate = useNavigate(); /// redirection
  const cart = useSelector((state) => state.cart.cartItems);
  const isLogged = useSelector((state) => state.authentication.isLogged);
  console.log(cart.length, "cartvalue");

  useEffect(() => {
    if (cart.length === 0) {
      navigate(paths.shop);
      console.log("emptytt");
      console.log("isloggedin ", isLogged);
    }
  }, [cart]);
  // get total price
  const getTotalPrice = () => {
    let totalPrice = 0;
    cart.forEach((productItems) => {
      totalPrice = totalPrice + productItems.quantity * productItems.price;
      // console.log(totalPrice, "total prices");
    });

    return totalPrice;
  };
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  const schema = yup
    .object()
    .shape({
      firstName: yup
        .string()
        .required("First name is required")
        .min(4, "Enter at least 4 characters"),
      lastName: yup.string().required("last name is required"),
      phoneNumber: yup
        .string()
        .matches(phoneRegExp, "Phone number is not valid"),
      email: yup
        .string()
        .email("Email must be a valid email")
        .required("Email is required"),
      country: yup.string().required("Country is required"),
      addressOne: yup.string().required("Address one is required"),
      addressTwo: yup.string().required("Address two is required"),
      town: yup.string().required("Town/City is required"),
    })
    .required();

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = (data) => {
    toast.success("Your order succesfully proceed", { autoClose: 3000 });
    localStorage.removeItem("cart");
    navigate(paths.home);
  };
  return (
    <Fragment>
      {" "}
      <BreadCrumb
        breadCrumbTitle="CheckOut"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
      {/* <!--================Checkout Area =================--> */}
      <section class="checkout_area section_gap">
        <div class="container">
          {isLogged ? (
            <p></p>
          ) : (
            <div class="returning_customer">
              <div class="check_title">
                <h2>
                  Returning Customer? &nbsp;
                  <NavLink
                    state={{ previewPath: paths.checkOut }}
                    to={paths.login}
                  >
                    Click here to login
                  </NavLink>
                </h2>
              </div>
              <p>
                If you have shopped with us before, please enter your details in
                the boxes below. If you are a new customer, please proceed to
                the Billing & Shipping section.
              </p>
            </div>
          )}
          {/* {console.log(isLogged, "loggedddd")} */}

          <div class="billing_details">
            <div class="row">
              <div class="col-lg-8">
                <h3>Billing Details</h3>
                <form
                  onSubmit={handleSubmit(onSubmit)}
                  class="row contact_form"
                  action="#"
                  method="post"
                  novalidate="novalidate"
                >
                  <div class="col-md-6 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="first"
                      name="name"
                      {...register("firstName")}
                      placeholder="First name"
                    />
                    <p className="error">{errors.firstName?.message}</p>
                  </div>
                  <div class="col-md-6 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="last"
                      name="name"
                      placeholder="Last Name"
                      {...register("lastName")}
                    />
                    <p className="error">{errors.lastName?.message}</p>
                  </div>
                  <div class="col-md-12 form-group">
                    <input
                      type="text"
                      class="form-control"
                      id="company"
                      name="company"
                      placeholder="Company name"
                      {...register("company")}
                    />
                  </div>
                  <div class="col-md-6 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="number"
                      name="phoneNumber"
                      {...register("phoneNumber")}
                      placeholder="Phone number"
                    />
                    <p className="error">{errors.phoneNumber?.message}</p>
                  </div>
                  <div class="col-md-6 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="email"
                      name="compemailany"
                      placeholder="Email Address"
                      {...register("email")}
                    />
                    <p className="error">{errors.email?.message}</p>
                  </div>
                  <div class="col-md-12 form-group p_star">
                    <select
                      class="nice-select country_select"
                      {...register("country")}
                    >
                      <option value="1">Country</option>
                      <option value="2">Country</option>
                      <option value="4">Country</option>
                    </select>
                    <p className="error">{errors.country?.message}</p>
                  </div>
                  <div class="col-md-12 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="add1"
                      name="add1"
                      placeholder="Address line 01"
                      {...register("addressOne")}
                    />
                    <p className="error">{errors.addressOne?.message}</p>
                  </div>
                  <div class="col-md-12 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="add2"
                      name="add2"
                      {...register("addressTwo")}
                      placeholder="Address line 02"
                    />
                    <p className="error">{errors.addressTwo?.message}</p>
                  </div>
                  <div class="col-md-12 form-group p_star">
                    <input
                      type="text"
                      class="form-control"
                      id="city"
                      name="city"
                      {...register("town")}
                      placeholder="Town/City"
                    />
                    <p className="error">{errors.town?.message}</p>
                  </div>
                  <div class="col-md-12 form-group p_star">
                    <select class="nice-select country_select">
                      <option value="1">District</option>
                      <option value="2">District</option>
                      <option value="4">District</option>
                    </select>
                  </div>
                  <div class="col-md-12 form-group">
                    <input
                      type="text"
                      class="form-control"
                      id="zip"
                      name="zip"
                      placeholder="Postcode/ZIP"
                      {...register("postcode")}
                    />
                  </div>
                  <div class="col-md-12 form-group">
                    <button className="primary-btn" type="submit">
                      {" "}
                      Proceed to buy
                    </button>
                  </div>
                </form>
              </div>
              <div class="col-lg-4">
                <div class="order_box">
                  <h2>Your Order</h2>
                  <ul class="list">
                    <li>
                      <a href="#">
                        Product <span>Total</span>
                      </a>
                    </li>
                    {cart.map((items) => {
                      console.log(items, "product items");

                      return (
                        <li>
                          <a href="javascript:;">
                            {items.title}{" "}
                            <span class="middle">x {items.quantity}</span>{" "}
                            <span class="last">
                              {items.price * items.quantity}
                            </span>
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                  <ul class="list list_2">
                    <li>
                      <a href="#">
                        Subtotal <span>${getTotalPrice()}</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        Shipping <span>Flat rate: $10.00</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        Total <span>${getTotalPrice() + 10}</span>
                      </a>
                    </li>
                  </ul>

                  <div class="payment_item active">
                    <div class="radion_btn">
                      {/* <input type="radio" id="f-option6" name="selector" />
                      <label for="f-option6">Cash on Delivery </label> */}
                      {/* <img src="img/product/card.jpg" alt="" /> */}
                      {/* <div class="check"></div> */}
                    </div>
                    <p>Cash on delivery available</p>
                  </div>
                  <div class="creat_account">
                    {/* <input type="checkbox" id="f-option4" name="selector" />
                    <label for="f-option4">I’ve read and accept the </label>
                    <a href="#">terms & conditions*</a> */}
                  </div>
                  {/* <a class="primary-btn" href="#">
                    Proceed to Paypal
                  </a> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!--================End Checkout Area =================--> */}
    </Fragment>
  );
};

export default CheckOut;
