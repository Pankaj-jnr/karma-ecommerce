import { Fragment } from "react";
import BreadCrumb from "../../../components/BreadCrumb";
import HomeProduct from "../shop/HomeProduct";
const Shop = () => {
  return (
    <Fragment>
      <BreadCrumb
        breadCrumbTitle="Shop"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />

      <section className="active-product-area section_gap">
        <div className="single-product-slider">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-6 text-center">
                <div className="section-title">
                  <h1>Latest Products</h1>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </p>
                </div>
              </div>
            </div>
            <HomeProduct />
          </div>
        </div>
      </section>
    </Fragment>
  );
};

export default Shop;
