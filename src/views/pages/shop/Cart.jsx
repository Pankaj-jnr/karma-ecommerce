import { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import BreadCrumb from "../../../components/BreadCrumb";
import { paths } from "../../../config/PathConfig";
import { CartAction } from "../../../store/cartSlice";
import { toast } from "react-toastify";
import ReactTooltip from "react-tooltip";
const Cart = () => {
  //   const [qty, setQty] = useState();
  const cart = useSelector((state) => state.cart.cartItems);
  const dispatch = useDispatch();
  const [disableCheckOut, setDisableCheckOut] = useState(false);

  //   const quantityHandler = (e) => {
  //     console.log(e.target.value, "quantity");
  //     setQty(e.target.value);
  //   };

  const deleteProductHandler = (items) => {
    dispatch(CartAction.removeFromCart(items));
    // console.log(items, "items id");
  };

  const decreaseCartHandler = (items) => {
    dispatch(CartAction.decreaseCart(items));
    // console.log(items, "decrese ");
  };

  const increaseCartHandler = (items) => {
    dispatch(CartAction.addToCart(items));
    // console.log(items, "decrese ");
  };

  const subTotal = () => {
    let price = 0;
    cart.forEach((items) => {
      // console.log(items, "products");
      price = price + items.quantity * items.price;
      // console.log(price, "price");
    });
    return price;
  };
  const quantityChangeHandler = ({ currentTarget }) => {
    const { id, value } = currentTarget;
    let qty = 0;

    if (value > 3) {
      toast.error("You can't add more than 3 items");
      qty = 3;
    } else if (value == 0) {
      console.log(value, "value 0");
      setDisableCheckOut(true);
    } else {
      setDisableCheckOut(false);

      qty = value;
    }

    // value > 8
    //   ? toast.error("You can't add more than 8 items")((qty = 8))
    //   : value == 0
    //   ? setDisableCheckOut(true)
    //   : (qty = value);
    dispatch(CartAction.quantityUpdate({ id: id, qty: qty }));
  };

  useEffect(() => {
    cart.forEach((item) => {
      item.quantity == 0 ? setDisableCheckOut(true) : <p></p>;
      // if (item.quantity == 0) {
      //   setDisableCheckOut(true);
      // }
    });
  });
  return (
    <Fragment>
      <BreadCrumb
        breadCrumbTitle="Shopping Cart"
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
      {/* <!--================Cart Area =================--> */}

      {cart.length === 0 ? (
        <div>
          <h2 align="center">Your cart is empty</h2>
        </div>
      ) : (
        <section class="cart_area">
          <div className="container">
            <div className="cart_inner">
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Product</th>
                      <th scope="col">Price</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Total</th>
                      <th scope="col">Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cart.map((items) => {
                      return (
                        // <tr key={items.id}>
                        <tr key={items.id}>
                          <td>
                            <div class="media">
                              <div class="d-flex">
                                <img src={`/${items.image}`} alt="" />
                              </div>
                              <div class="media-body">
                                <p>{items.description}</p>
                              </div>
                            </div>
                          </td>
                          <td>
                            <h5>${items.price}</h5>
                          </td>
                          <td>
                            <div class="product_count">
                              <input
                                type="number"
                                name="qty"
                                min="1"
                                title="Quantity:"
                                value={items.quantity + ""}
                                id={+items.id}
                                onChange={quantityChangeHandler}
                              />
                              {/* <button
                                onClick={() => increaseCartHandler(items)}
                                class="increase items-count "
                                type="button"
                              >
                                <i class="lnr lnr-chevron-up"></i>
                              </button>
                              <button
                                onClick={() => decreaseCartHandler(items)}
                                class="reduced items-count sdfsdfsdf"
                                type="button"
                              >
                                <i class="lnr lnr-chevron-down"></i>
                              </button> */}
                            </div>
                          </td>
                          <td>
                            <h5>{items.price * items.quantity}</h5>
                          </td>
                          <td>
                            <h5>
                              <button
                                onClick={() => deleteProductHandler(items)}
                                className="lnr lnr-cross"
                              ></button>
                            </h5>
                          </td>
                        </tr>
                      );
                    })}
                    {/* <tr class="bottom_button">
                      <td>
                        <a class="gray_btn" href="#">
                          Update Cart
                        </a>
                      </td>
                      <td></td>
                      <td></td>
                      <td>
                        <div class="cupon_text d-flex align-items-center">
                          <input type="text" placeholder="Coupon Code" />
                          <a class="primary-btn" href="#">
                            Apply
                          </a>
                          <a class="gray_btn" href="#">
                            Close Coupon
                          </a>
                        </div>
                      </td>
                      <td></td>
                    </tr> */}
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>
                        <h5>Subtotal</h5>
                      </td>
                      <td>
                        <h5>{subTotal()}</h5>
                      </td>
                    </tr>

                    <tr class="out_button_area">
                      <td></td>
                      <td></td>

                      <td>
                        <div class="checkout_btn_inner d-flex align-items-center">
                          <NavLink className="gray_btn" to={paths.shop}>
                            Continue Shopping
                          </NavLink>
                          {/* <NavLink
                            disabled={disableCheckOut}
                            className="primary-btn"
                            to={paths.checkOut}
                          >
                            Proceed to checkout
                          </NavLink> */}

                          {/* <button
                            disabled={disableCheckOut}
                            className="primary-btn"
                          >
                            Proceed to checkout
                          </button> */}
                          {!disableCheckOut ? (
                            <Fragment>
                              {" "}
                              <NavLink
                                className="primary-btn"
                                to={paths.checkOut}
                              >
                                Proceed to checkout
                              </NavLink>{" "}
                            </Fragment>
                          ) : (
                            <Fragment>
                              <button
                                data-tip="You need to add the quantity at list one"
                                // onMouseEnter={mouseEnterHandler()}
                                disabled={disableCheckOut}
                                className="primary-btn"
                              >
                                Proceed to checkout
                              </button>
                              <ReactTooltip />

                              {/* <p>You need to increase </p> */}
                            </Fragment>
                          )}
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      )}
      {/* <!--================End Cart Area =================--> */}
    </Fragment>
  );
};

export default Cart;
