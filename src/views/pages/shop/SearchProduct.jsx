import { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router";
import { NavLink } from "react-router-dom";
import BreadCrumb from "../../../components/BreadCrumb";
import services from "../../../services/services";
import PrdouctFilter from "../../../utility/shop/productFilter";
import { Link } from "react-router-dom";
import classes from "../../../utility/shop/filter.module.css";
const SearchProduct = () => {
  const { cat } = useParams();
  const [products, setProducts] = useState([]);
  const [isActive, setIsActive] = useState(false);
  const [isHigthActive, setisHigthActive] = useState(false);

  const lowToHighPriceHanlder = () => {
    const sorting = products.sort((a, b) => a.price - b.price);
    setIsActive(true);
    setisHigthActive(false);
    return setProducts(sorting);
  };

  const highToLowPriceHandler = () => {
    const sorting = products.sort((a, b) => b.price - a.price);
    setisHigthActive(true);
    setIsActive(false);
    return setProducts(sorting);
  };
  // return setProducts(a.price - b.price);
  useEffect(() => {
    const productDB = services.getAllProducts();
    const filterProduct = productDB.filter((item) => item.cat === cat);
    setIsActive(true);

    setProducts(filterProduct);
  }, []);
  return (
    <Fragment>
      <BreadCrumb
        breadCrumbTitle={cat}
        breadCrumbParent="Home"
        breadCrumbPath="/"
      />
      <section className="active-product-area section_gap">
        <div className="container">
          <div className={classes["product-filter-bar"]}>
            <h5>
              Showing {products.length} results for "{cat}"{" "}
            </h5>
            <ul>
              <li>
                <b>Sort By </b>
              </li>
              <li>
                <NavLink
                  className={isActive ? classes.active : ""}
                  to="#"
                  onClick={() => lowToHighPriceHanlder()}
                >
                  Price -- Low to High
                </NavLink>
              </li>
              <li>
                <NavLink
                  className={isHigthActive ? classes.active : ""}
                  to="#"
                  onClick={() => highToLowPriceHandler()}
                >
                  {" "}
                  Price -- High to Low
                </NavLink>
              </li>
            </ul>
          </div>
        </div>

        <div className="single-product-slider">
          <div className="container">
            <div className="row justify-content-left">
              <div className="col-lg-12 text-center">
                <div className="section-title">
                  {/* <h1>Serach Result for {cat}</h1> */}
                </div>
              </div>
              {products.map((items) => {
                return (
                  <div className="col-lg-3 col-md-6" key={items.id}>
                    <div className="single-product">
                      <NavLink to={`/prodct-details/${items.id} `}>
                        <img
                          className="img-fluid"
                          src={`/${items.image}`}
                          alt=""
                        />
                        <div className="product-details">
                          <h6>{items.title}</h6>
                          <div className="price">
                            <h6>{`$ ${items.price}`}</h6>
                            {/* <h6 className="l-through">$210.00</h6> */}
                          </div>
                          <div className="prd-bottom">
                            <NavLink
                              to={`/prodct-details/${items.id} `}
                              className="social-info"
                            >
                              <span className="ti-bag"></span>
                              <p className="hover-text">add to bag</p>
                            </NavLink>
                          </div>
                        </div>
                      </NavLink>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

export default SearchProduct;
