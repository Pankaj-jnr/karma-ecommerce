import { Fragment, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import services from "../../../services/services";
import { paths } from "../../../config/PathConfig";
import { useDispatch } from "react-redux";
import { getProducts } from "../../../store/productSlice";
const HomeProduct = () => {
  const [products, setproducts] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    const dbProducts = services.getAllProducts();
    dispatch(getProducts(dbProducts));
    setproducts(dbProducts);
  }, []);

  return (
    <Fragment>
      <div className="row">
        {products.map((items) => {
          // console.log(items, "products items");

          return (
            <div className="col-lg-3 col-md-6">
              <div className="single-product">
                <NavLink to={`/prodct-details/${items.id} `}>
                  <img className="img-fluid" src={items.image} alt="" />
                  <div className="product-details">
                    <h6>{items.title}</h6>
                    <div className="price">
                      <h6>{`$ ${items.price}`}</h6>
                      <h6 className="l-through">$210.00</h6>
                    </div>
                    <div className="prd-bottom">
                      <NavLink
                        to={`/prodct-details/${items.id} `}
                        className="social-info"
                      >
                        <span className="ti-bag"></span>
                        <p className="hover-text">add to bag</p>
                      </NavLink>
                      {/* <a href="" className="social-info">
                        <span className="lnr lnr-heart"></span>
                        <p className="hover-text">Wishlist</p>
                      </a>
                      <a href="" className="social-info">
                        <span className="lnr lnr-sync"></span>
                        <p className="hover-text">compare</p>
                      </a>
                      <a href="" className="social-info">
                        <span className="lnr lnr-move"></span>
                        <p className="hover-text">view more</p>
                      </a> */}
                    </div>
                  </div>
                </NavLink>
              </div>
            </div>
          );
        })}
      </div>
    </Fragment>
  );
};

export default HomeProduct;
