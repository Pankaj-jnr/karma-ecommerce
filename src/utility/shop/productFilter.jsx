import { Fragment } from "react";
import { Link } from "react-router-dom";
import classes from "../shop/filter.module.css";
const PrdouctFilter = () => {
  return (
    <Fragment>
      <div className="container">
        <div className={classes["product-filter-bar"]}>
          <h5>Showing 1 – 40 of 1,021 results for "air cooler"</h5>
          <ul>
            <li>
              <b>Sort By </b>
            </li>
            <li>
              <Link to=""> Price -- Low to High</Link>
            </li>
            <li>
              <Link to=""> Price -- High to Low </Link>
            </li>
          </ul>
        </div>
      </div>
    </Fragment>
  );
};

export default PrdouctFilter;
