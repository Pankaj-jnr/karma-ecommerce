import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const useToast = () => {
  const toastMessage = (data) => {
    if (data.status) {
      return toast.success(data.message);
    } else {
      return toast.error(data.message);
    }
  };
  return toastMessage;
};

export default useToast;
