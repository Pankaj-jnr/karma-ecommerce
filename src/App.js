import "./App.css";
import Header from "./layout/header/Header";
import { Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import KMRoutes from "./router/routesConfig";
import Footer from "./layout/footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function App() {
  return (
    <div className="App">
      <ToastContainer />
      <Header />
      <Suspense fallback={<p>Loading</p>}>
        <Routes>
          {KMRoutes.map((item) => {
            return <Route path={item.path} element={<item.element />} />;
          })}
        </Routes>
      </Suspense>
      <Footer />
    </div>
  );
}

export default App;
