import { Fragment } from "react"
import {NavLink} from 'react-router-dom'
import { paths } from "../config/PathConfig";


const BreadCrumb = ({breadCrumbTitle, breadCrumbParent, breadCrumbPath}) => {
    return ( 
    <Fragment>
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>{breadCrumbTitle}</h1>
                        <nav class="d-flex align-items-center">
                            <NavLink to={breadCrumbPath}>{breadCrumbParent}<span class="lnr lnr-arrow-right"></span></NavLink>
                            <NavLink to='/'>{breadCrumbTitle}</NavLink>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </Fragment>
    )
}
 
export default BreadCrumb;