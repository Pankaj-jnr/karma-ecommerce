import { configureStore } from "@reduxjs/toolkit";
import authSlice from "../store/auth";
import productSlice from "./productSlice";
import cartSlice from "./cartSlice";
import dummySlice from "./dummy";
export const store = configureStore({
  reducer: {
    authentication: authSlice,
    products: productSlice,
    cart: cartSlice,
    dummy: dummySlice,
  },
});
