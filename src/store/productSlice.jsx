import { createSlice } from "@reduxjs/toolkit";

export const productSlice = createSlice({
  name: "products",
  initialState: {
    items: [],
    users: [],
  },
  reducers: {
    getProducts: (state, action) => {
      console.log(state.items, "before ");
      state.items = action.payload;
      console.log(state.items, "after");
      // console.log(action.payload)
    },
  },
});

export const { getProducts } = productSlice.actions;
export default productSlice.reducer;
