import { createSlice } from "@reduxjs/toolkit";

const initialUser = () => {
  const item = localStorage.getItem("userData");
  return item ? JSON.parse(item) : { email: "" };
};
export const authSlice = createSlice({
  name: "authentication",
  initialState: {
    data: localStorage.getItem("userData")
      ? JSON.parse(localStorage.getItem("userData"))
      : {},
    isLogged: localStorage.getItem("userData") ? true : false,
  },

  reducers: {
    loginHandle: (state, action) => {
      state.data = action.payload;
      console.log("store");
      localStorage.setItem("userData", JSON.stringify(action.payload));
      state.isLogged = true;
    },

    logOutHandle: (state) => {
      state.data = {};
      state.isLogged = false;
      localStorage.removeItem("userData");
    },
  },
});
export const { loginHandle, logOutHandle } = authSlice.actions; //export login action

export default authSlice.reducer;
