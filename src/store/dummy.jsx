import { createSlice } from "@reduxjs/toolkit";

export const dummySlice = createSlice({
  name: "dummy",
  initialState: {
    data: [],
    total: 1,
  },
  reducers: {
    logInUser: (state, action) => {
      state.data = action.payload;
    },
  },
});

export const { dummySliceAction } = dummySlice.actions;
export default dummySlice.reducer;
