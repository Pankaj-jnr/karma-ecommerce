import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cartItems: localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [],
    quantity: 0,
  },
  reducers: {
    addToCart: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item.id == action.payload.id
      );
      if (itemIndex >= 0) {
        state.cartItems[itemIndex].quantity =
          state.cartItems[itemIndex].quantity + 1;
      } else {
        const tempProduct = { ...action.payload, quantity: 1 };
        state.cartItems.push(tempProduct);
      }
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
    },

    removeFromCart: (state, action) => {
      const updatedCart = state.cartItems.filter(
        (item) => item.id !== action.payload.id
      );
      if (updatedCart.length == 0) {
        state.cartItems = [];
        localStorage.removeItem("cart");
      } else {
        state.cartItems = updatedCart;
        localStorage.setItem("cart", JSON.stringify(state.cartItems));
      }
    },

    decreaseCart: (state, action) => {
      const itemIndex = state.cartItems.findIndex(
        (item) => item.id === action.payload.id
      );
      if (state.cartItems[itemIndex].quantity > 1) {
        state.cartItems[itemIndex].quantity -= 1;
        console.log("greater than i");
      } else if (state.cartItems[itemIndex].quantity === 1) {
        const compareItem = state.cartItems.filter(
          (item) => item.id !== action.payload.id
        );
        state.cartItems = compareItem;
      }
      console.log(itemIndex, "itemindex");
      localStorage.setItem("cart", JSON.stringify(state.cartItems));
    },

    quantityUpdate: (state, action) => {
      let qty = +action.payload.qty;
      console.log(typeof qty, "type of");
      const cart = JSON.parse(localStorage.getItem("cart"));
      const cartItems = cart.map((cartItem) => {
        // if (qty == 0) {
        //   return { ...cartItem, quantity: qty + 1 };
        // }
        if (cartItem.id === action.payload.id) {
          return { ...cartItem, quantity: qty };
        } else {
          return cartItem;
        }
      });
      localStorage.setItem("cart", JSON.stringify(cartItems));
      state.cartItems = cartItems;
    },

    // updatedCart: (state, action) => {
    //   // console.log(action.payload, "payload working");
    //   state.cartItems = action.payload.cartUpdate;
    // },
  },
});

export const CartAction = cartSlice.actions;
export default cartSlice.reducer;
