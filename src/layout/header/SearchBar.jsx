import { Fragment, useState } from "react";
import { NavLink } from "react-router-dom";
import { paths } from "../../config/PathConfig";
import services from "../../services/services";

const SearchBar = () => {
  const [filterProduct, setfilterProduct] = useState([]);
  const searchBarHandler = ({ currentTarget }) => {
    const { value } = currentTarget;
    const dbProducts = services.getCat();
    const filterProduct = dbProducts.filter((item) => {
      if (item.cat.startsWith(value) && value != "") {
        return item;
      }
    });
    setfilterProduct(filterProduct);
  };

  return (
    <Fragment>
      <div className="search_input" id="search_input_box">
        <div className="container">
          <form className="d-flex justify-content-between" action="">
            <input
              type="text"
              className="form-control"
              id="search_input"
              placeholder="Search Here"
              onChange={searchBarHandler}
            />
            <button type="submit" className="btn"></button>
            <span
              className="lnr lnr-cross"
              id="close_search"
              title="Close Search"
            ></span>
            {filterProduct.length > 0 && (
              <div className="searchResult">
                <ul>
                  {filterProduct.map((item) => {
                    return (
                      <li>
                        <NavLink to={`/search/${item.cat}`}>
                          <div className="productImg">
                            <img src={`/${item.image}`} />
                          </div>
                          {item.title}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
          </form>
        </div>
      </div>
    </Fragment>
  );
};

export default SearchBar;
