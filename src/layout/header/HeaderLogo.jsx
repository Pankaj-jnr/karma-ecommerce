import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { paths } from "../../config/PathConfig";

const HeaderLogo = () => {
  return (
    <Fragment>
      <NavLink className="navbar-brand logo_h" to={paths.home}>
        <img src="/assets/images/logo.png" alt="" />
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </button>
    </Fragment>
  );
};

export default HeaderLogo;
