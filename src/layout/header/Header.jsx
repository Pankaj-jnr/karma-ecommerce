import { Fragment, useEffect } from "react";
import HeaderLogo from "./HeaderLogo";
import MainMenu from "../../navigation/Navigation";
import HeaderMenu from "./HeaderMenu";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../store/auth";
import { isUserLoggedIn } from "../../utility/Utils";
import { NavLink, useNavigate } from "react-router-dom";
import { paths } from "../../config/PathConfig";
import SearchBar from "./SearchBar";
const Header = () => {
  console.log(isUserLoggedIn, "user login");
  const loggedIn = localStorage.getItem("userData");
  const cart = useSelector((state) => state.cart.cartItems);

  // useEffect(() => {
  const totalQty = () => {
    let totalItems = 0;
    cart.forEach((products) => {
      const qty = +products.quantity;
      totalItems = totalItems + qty;
    });
    return totalItems;
  };
  // });
  console.log(totalQty(), "total");
  // const totalItems = cart.length

  useEffect(() => {}, []);

  const isAuth = useSelector((state) => state.authentication.data);

  return (
    <Fragment>
      <header className="header_area sticky-header">
        <div className="main_menu">
          <nav className="navbar navbar-expand-lg navbar-light main_box">
            <div className="container">
              {/* Brand and toggle get grouped for better mobile display */}
              <HeaderLogo />
              {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
              <div
                className="collapse navbar-collapse offset"
                id="navbarSupportedContent"
              >
                <HeaderMenu HeaderMenu={MainMenu} />
                <ul className="nav navbar-nav navbar-right">
                  <li className="nav-item">
                    <NavLink to={paths.cart} className="cart">
                      <span className="ti-bag">
                        <span className="badge badge-warning">
                          {totalQty()}
                        </span>
                      </span>
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <button className="search">
                      <span className="lnr lnr-magnifier" id="search"></span>
                    </button>
                  </li>

                  {/* {loggedIn ? "logged" : "NotLog"} &nbsp;
                  {isAuth ? "loggedIn" : "notlogin"} */}
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <SearchBar />
      </header>
    </Fragment>
  );
};
export default Header;
