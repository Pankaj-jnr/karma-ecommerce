import { Fragment } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { logindetails } from "../../utility/Utils";
import { useDispatch, useSelector } from "react-redux";
import LogOut from "../../views/pages/auth/LogOut";
const HeaderMenu = ({ HeaderMenu }) => {
  const isLogged = useSelector((state) => state.authentication.data);
  // console.log(isLogged, "loggedd");
  // const navigate = useNavigate();
  // const dispatch = useDispatch();

  const loggedIn = localStorage.getItem("userData");
  // console.log(loggedIn, "loginnnnnn");
  return (
    <Fragment>
      <ul className="nav navbar-nav menu_nav ml-auto">
        {HeaderMenu.map((item) => {
          if (item.hasOwnProperty("path")) {
            if (item.auth) {
              // console.log("inside auth item.checkout ", item);
              if (item.checkAuth) {
                return (
                  <Fragment>
                    {loggedIn && (
                      <li className="nav-item" key={item.id}>
                        <NavLink
                          className="nav-link dropdown-toggle"
                          to={item.path}
                        >
                          {item.title}
                        </NavLink>
                      </li>
                    )}
                  </Fragment>
                );
              } else {
                return (
                  <Fragment>
                    {!loggedIn && (
                      <li className="nav-item" key={item.id}>
                        <NavLink
                          className="nav-link dropdown-toggle"
                          to={item.path}
                        >
                          {item.title}
                        </NavLink>
                      </li>
                    )}
                  </Fragment>
                );
              }
            } else {
              return (
                <li className="nav-item" key={item.id}>
                  <NavLink className="nav-link dropdown-toggle" to={item.path}>
                    {item.title}
                  </NavLink>
                </li>
              );
            }
          } else if (item.hasOwnProperty("children")) {
            console.log("children");
            return (
              <li className="nav-item submenu dropdown" key={item.id}>
                <NavLink
                  to="#"
                  // className=""
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link nav-link  active"
                      : "nav-link  dropdown-toggle"
                  }
                >
                  {item.title}
                </NavLink>
                <ul className="dropdown-menu">
                  {item.children.map((submenu) => {
                    // console.log(item, "sdfsdf");
                    return (
                      <li className="nav-item" key={submenu.id}>
                        <NavLink to={submenu.path}>{submenu.title}</NavLink>
                      </li>
                    );
                  })}
                </ul>
              </li>
            );
          }
        })}
      </ul>
    </Fragment>
  );
};

export default HeaderMenu;
