export const shop = [
  {
    id: "1",
    image: "assets/images/product/p6.jpg",
    title: "addidas New Hammer sole",
    price: 150,
    cat: "addidas",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "2",
    image: "assets/images/product/p7.jpg",
    title: "nike New hammer sole",
    price: 140,
    cat: "nike",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "3",
    image: "assets/images/product/p7.jpg",
    title: "addidas New Hammer sole",
    price: 130,
    cat: "addidas",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },
  {
    id: "4",
    image: "assets/images/product/p8.jpg",
    title: "addidas New Hammer sole",
    price: 120,
    cat: "addidas",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "5",
    image: "assets/images/product/p6.jpg",
    title: "nike 1",
    price: 110,
    cat: "nike",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "6",
    image: "assets/images/product/p7.jpg",
    title: "nike 2",
    price: 122,
    cat: "nike",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "7",
    image: "assets/images/product/p7.jpg",
    title: "nike 3",
    price: 121,
    cat: "nike",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },
  {
    id: "8",
    image: "assets/images/product/p8.jpg",
    title: "nike 4",
    price: 120,
    cat: "nike",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },

  {
    id: "9",
    image: "assets/images/product/bata/bata-shoe1.jpg",
    title: "bata 1",
    price: 200,
    cat: "bata",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna",
  },
];
